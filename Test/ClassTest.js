class Car {
    constructor(make, model) {
        this.make = make;
        this.model = model;
        this.speed = 0;
    }
    get make() {
        return this._make;
    }
    set make(make) {
        this._make = make;
    }
    set model(model) {
        this._model = model;
    }
    get model() {
        return this._model;
    }
}

var car = new Car("Volvo", "Car Thingy");
console.log(car.make)
console.log(car.model)
car.model = "Hybrid"
car.make = "Toyota"
console.log("\n")
console.log(car.make)
console.log(car.model)
console.log("\n")
var myArray = [
    new Car("Yamaha", "Vstar-650"), new Car("Mazda", "Protege"), new Car("Nissan", "Sentra")
]
myArray.forEach(item => console.log(item.make));
