   var fileExt = [".png", ".jpg", ".jepg"];

   var counter = 0;
   $.ajax({
       //This will retrieve the contents of the folder if the folder is configured as 'browsable'
       url: 'LW',
       success: function (data) {
           var table = document.createElement("div");
           //List all png or jpg or gif file names in the page
           $(data).find("a:contains(" + fileExt[0] + "),a:contains(" + fileExt[1] + "),a:contains(" + fileExt[2] + ")").each(function () {
               var divs = document.createElement("div")
               var filename = this.href.replace(window.location.host, "").replace("http:///", "");
               console.log(filename);
               divs.className = "pannels";
               divs.style.backgroundImage = "url(" + "./" +
                   filename + ")";
               table.appendChild(divs);

           });
           var divContainer = document.getElementById("filenames");

           divContainer.innerHTML = "";
           divContainer.appendChild(table);
           table.className = "fileNames"
       }
   });
